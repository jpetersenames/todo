module github.com/found-it/todoclient

go 1.14

require (
	github.com/fatih/color v1.9.0
	github.com/mitchellh/go-homedir v1.1.0
	github.com/spf13/cobra v1.0.0
	github.com/spf13/viper v1.7.0
	gopkg.in/yaml.v2 v2.3.0
)
